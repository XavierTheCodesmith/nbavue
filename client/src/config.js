const MODE = process.env.NODE_ENV;

export default {
  API:
    MODE === "development"
      ? "http://127.0.0.1:5000/"
      : "https://nbavue.herokuapp.com/"
};
