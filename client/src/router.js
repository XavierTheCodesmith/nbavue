import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Teams from "./views/Teams.vue";
import Players from "./views/Players.vue";
import PlayerDetail from "./views/PlayerDetail.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/teams",
      name: "teams",
      component: Teams
    },
    {
      path: "/players",
      name: "players",
      component: Players
    },
    {
      path: "/detail/:playerId",
      name: "playerDetail",
      component: PlayerDetail
    }
  ]
});
