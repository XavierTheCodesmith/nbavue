import { basicStats } from "@/constants";

export const basicStatItemMixin = {
  computed: {
    basicStatItem() {
      return basicStats.map(stat => {
        const { perMode, player } = this;
        let header;
        let subHead;
        let isPercent = false;

        switch (perMode) {
          case "PerGame":
            subHead = "per game";
            break;
          case "Per36":
            subHead = "per 36";
            break;
          default:
            subHead = "";
            break;
        }

        switch (stat) {
          case "PTS":
            header = "Points";
            break;
          case "AST":
            header = "Assists";
            break;
          case "BLK":
            header = "Blocks";
            break;
          case "FT_PCT":
            header = "Free Throw Percentage";
            subHead = "%";
            isPercent = true;
            break;
          case "STL":
            header = "Steals";
            break;
          case "REB":
            header = "Rebounds";
            break;
          case "OREB":
            header = "Offensive Rebounds";
            break;
          case "DREB":
            header = "Defensive Rebounds";
            break;
          case "FGA":
            header = "Field Goal Attempts";
            break;
          case "FGM":
            header = "Field Goals Made";
            break;
          case "FTA":
            header = "Free Throw Attempts";
            break;
          case "FTM":
            header = "Free Throws Made";
            break;
          case "FG_PCT":
            header = "Field Goal Percentage";
            subHead = "%";
            isPercent = true;
            break;
          case "FG3_PCT":
            header = "3PT Field Goal Percentage";
            subHead = "%";
            isPercent = true;
            break;
          case "FG3M":
            header = "3PT Field Goals Made";
            break;
          case "FG3A":
            header = "3PT Field Goal Attempts";
            break;
          case "GS":
            header = "Games Started";
            break;
          case "GP":
            header = "Games Played";
            break;
          case "TOV":
            header = "Turnovers";
            break;
          case "PF":
            header = "Personal Fouls";
            break;
          default:
            header = "N/A";
        }
        return {
          header,
          stat: isPercent
            ? (player.career_stats[stat] * 100).toFixed(2)
            : player.career_stats[stat],
          subHead
        };
      });
    }
  }
};
