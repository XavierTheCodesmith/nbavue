import { NBA_API_HEADERS } from "../constants";

const statsTableMixin = {
  computed: {
    regSeasonTotalsHeaders() {
      const { regTotals, makeTableHeaders, orderHeaderData } = this;
      return (
        regTotals &&
        regTotals.length &&
        makeTableHeaders(orderHeaderData(regTotals[0]))
      );
    },
    regSeasonTotalsRows() {
      const { regTotals, makeTableRows, regSeasonTotalsHeaders } = this;
      return (
        regTotals &&
        regTotals.length &&
        regSeasonTotalsHeaders &&
        makeTableRows(regSeasonTotalsHeaders, regTotals)
      );
    },
    regSeasonPerGameHeaders() {
      const { regPerGame, makeTableHeaders, orderHeaderData } = this;

      return (
        regPerGame &&
        regPerGame.length &&
        makeTableHeaders(orderHeaderData(regPerGame[0]))
      );
    },
    regSeasonPerGameRows() {
      const { regPerGame, regSeasonPerGameHeaders, makeTableRows } = this;
      return (
        regPerGame &&
        regPerGame.length &&
        regSeasonPerGameHeaders &&
        makeTableRows(regSeasonPerGameHeaders, regPerGame)
      );
    },
    postSeasonTotalsHeaders() {
      const { postSeasonTotals, makeTableHeaders, orderHeaderData } = this;
      return (
        postSeasonTotals &&
        postSeasonTotals.length &&
        makeTableHeaders(orderHeaderData(postSeasonTotals[0]))
      );
    },
    postSeasonTotalsRows() {
      const { postSeasonTotals, postSeasonTotalsHeaders, makeTableRows } = this;
      return (
        postSeasonTotals &&
        postSeasonTotals.length &&
        postSeasonTotalsHeaders &&
        makeTableRows(postSeasonTotalsHeaders, postSeasonTotals)
      );
    },
    postSeasonPerGameHeaders() {
      const { postSeasonPerGame, makeTableHeaders, orderHeaderData } = this;
      return (
        postSeasonPerGame &&
        postSeasonPerGame.length &&
        makeTableHeaders(orderHeaderData(postSeasonPerGame[0]))
      );
    },
    postSeasonPerGameRows() {
      const {
        postSeasonPerGame,
        postSeasonPerGameHeaders,
        makeTableRows
      } = this;
      return (
        postSeasonPerGame &&
        postSeasonPerGame.length &&
        postSeasonPerGameHeaders &&
        makeTableRows(postSeasonPerGameHeaders, postSeasonPerGame)
      );
    }
  },
  methods: {
    orderHeaderData(data) {
      let sortedHeaders = {};

      for (let stat in NBA_API_HEADERS) {
        sortedHeaders[stat] = data[stat];
      }

      return sortedHeaders;
    },
    makeTableHeaders(stats) {
      const keys = Object.keys(stats);
      return keys
        .filter(k => NBA_API_HEADERS[k])
        .map(k => ({ name: NBA_API_HEADERS[k], stat: k }));
    },
    makeTableRows(headers, data) {
      return data.map(item => {
        return headers.map(h => item[h.stat]);
      });
    }
  }
};

export default statsTableMixin;
