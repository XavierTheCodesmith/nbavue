from nba_api.stats.static.players import get_players
from flask import Blueprint, jsonify, make_response
from nba_api.stats.endpoints import commonteamroster, playerprofilev2, commonplayerinfo
import datetime

players_bp = Blueprint('players_bp', __name__, url_prefix='/players')


@players_bp.route('/all', methods=['GET', ])
def get_all_players():
    return jsonify(get_players())


@players_bp.route('/roster/<team>', methods=['GET', ])
def get_current_roster(team):
    season_start = datetime.datetime.now().year
    season_end = datetime.datetime.now().year + 1

    season_str = '{}-{}'.format(season_start, str(season_end)[2:])
    try:
        team_dict = commonteamroster.CommonTeamRoster(
            team_id=team, season=season_str).common_team_roster.get_dict()
        headers = team_dict['headers']
        players = team_dict['data']
        roster = [dict(zip(headers, player)) for player in players]

        return jsonify(roster)
    except:
        return make_response('Team {} not found'.format(team))


@players_bp.route('/player/career/<player_id>', methods=['GET', ])
def get_player_career_info(player_id):
    try:
        career_high_dict = playerprofilev2.PlayerProfileV2(player_id=player_id,
                                                           per_mode36='PerGame').career_highs.get_dict()
        career_high_headers = career_high_dict['headers']
        career_high_data = career_high_dict['data']
        career_highs = [dict(zip(career_high_headers, data)) for data in career_high_data]

        regular_season_totals_dict = playerprofilev2.PlayerProfileV2(
            player_id=player_id, per_mode36='Totals').season_totals_regular_season.get_dict()
        regular_season_headers = regular_season_totals_dict['headers']
        regular_season_data = regular_season_totals_dict['data']
        regular_season_totals = [dict(zip(regular_season_headers, data)) for data in regular_season_data]

        regular_season_per_game_dict = playerprofilev2.PlayerProfileV2(player_id=player_id,
                                                                       per_mode36='PerGame').season_totals_regular_season.get_dict()
        regular_season_per_game_headers = regular_season_per_game_dict['headers']
        regular_season_per_game_data = regular_season_per_game_dict['data']
        regular_season_per_game = [dict(zip(regular_season_per_game_headers, data)) for data in
                                   regular_season_per_game_data]

        post_season_totals_dict = playerprofilev2.PlayerProfileV2(
            player_id=player_id, per_mode36='Totals').season_totals_post_season.get_dict()
        post_season_headers = post_season_totals_dict['headers']
        post_season_data = post_season_totals_dict['data']
        post_season_totals = [dict(zip(post_season_headers, data)) for data in post_season_data]

        post_season_per_game_dict = playerprofilev2.PlayerProfileV2(player_id=player_id,
                                                                    per_mode36='PerGame').season_totals_post_season.get_dict()
        post_season_per_game_headers = post_season_per_game_dict['headers']
        post_season_per_game_data = post_season_per_game_dict['data']
        post_season_per_game = [dict(zip(post_season_per_game_headers, data)) for data in post_season_per_game_data]

        return jsonify({'regular_totals': regular_season_totals, 'post_season_totals': post_season_totals,
                        'regular_per_game': regular_season_per_game, 'post_season_per_game': post_season_per_game,
                        'career_highs': career_highs})
    except:
        return make_response('Player {} not found'.format(player_id))


@players_bp.route('/player/basic/<player_id>/<per_mode>', methods=['GET', ])
def get_basic_player_info(player_id, per_mode):
    try:
        career_stats_dict = playerprofilev2.PlayerProfileV2(
            player_id=player_id, per_mode36=per_mode).career_totals_regular_season.get_dict()
        career_stats_headers = career_stats_dict['headers']
        career_stats_data = career_stats_dict['data']
        career_stats = [dict(zip(career_stats_headers, stat))
                        for stat in career_stats_data]

        player_dict = commonplayerinfo.CommonPlayerInfo(
            player_id=player_id).common_player_info.get_dict()
        player_headers = player_dict['headers']
        player_data = player_dict['data']
        player = [dict(zip(player_headers, p)) for p in player_data]

        return jsonify({'career_stats': career_stats[0], 'player': player[0]})
    except:
        return make_response('Player {} not found'.format(player_id))
